import React, { useState } from "react";
import { validName, validAge } from "./Regex";

const User = () => {
  const [data, setData] = useState({
    firstname: "", // Here firstName -> event.target.firstName.value
    lastname: "",
    age: "",
    notes: "",
    color: "",
    radio: "Larry",
  });

  const [isEmployed, setIsEmployed] = useState(false);
  const [checkedList, setCheckedList] = useState([]);
  const [btnDisabled, setBtnDisabled] = useState(false);
  const [nameError, setNameError] = useState(false);

  const jsonData = {
    data,
    Souces: checkedList,
    Employed: isEmployed,
  };

  const checkItems = ["Ketchup", "Mustard", "Mayonnaise", "Guacamole"];
  const radioItems = ["Larry", "Moe", "Curly"];

  const validateName = () => {
    if(!validName.test(data.firstname) || !data.firstname) {
      alert("Name is not valid")
    } 
  }

  const validateLastName = () => {
    if(!validName.test(data.lastname) || !data.lastname) {
      alert("LastName is not valid")
    } 
  }

  const validateAge = () => {
    if(!validAge.test(data.age) || !data.age) {
      alert("Age is not valid")
    } 
  }


  const handleIsEmployeedChange = (event) => {
    if (event.target.checked) {
      console.log("✅ Checkbox is checked");
      setBtnDisabled(false);
    } else {
      console.log("⛔️ Checkbox is NOT checked");
      setBtnDisabled(true);
    }
    setIsEmployed((current) => !current);
    setBtnDisabled(false);
  };

  const handleCheckListChange = (event) => {
    let updatedList = [...checkedList];
    if (event.target.checked) {
      updatedList = [...checkedList, event.target.value];
      setBtnDisabled(false);
    } else {
      updatedList.splice(checkedList.indexOf(event.target.value), 1);
      setBtnDisabled(true);
    }

    setCheckedList(updatedList);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    console.log(jsonData);

    setNameError(validateName());
    setNameError(validateLastName());
    setNameError(validateAge());
  };

  const handleInputField = (e, name) => {
    if (e.target.value.length < 1) {
      setData({ ...data, [name]: e.target.value });
      setBtnDisabled(true);
    } else {
      setData({ ...data, [name]: e.target.value });
      setBtnDisabled(false);
    }
  };

  const handleResetButton = () => {
    setData("");
    setIsEmployed(false);
    setCheckedList([]);
    setBtnDisabled(true);
  };



  return (
    <div className="wrapper">
      <form
        className="form"
        onSubmit={handleFormSubmit}
        onReset={handleResetButton}
      >
        <div className="field-box">
          <label>First Name</label>
          <input
            type="text"
            name="first_name"
            placeholder="First Name"
            value={data.firstname}
            onChange={(e) => {
              handleInputField(e, "firstname");
            }}
          />
        </div>
        <div className="field-box">
          <label>Last Name</label>
          <input
            type="text"
            name="last_name"
            placeholder="Last Name"
            value={data.lastname}
            onChange={(e) => {
              handleInputField(e, "lastname");
            }}
          />
        </div>
        <div className="field-box">
          <label>Age</label>
          <input
            type="text"
            name="last_name"
            placeholder="Age"
            value={data.age}
            onChange={(e) => {
              handleInputField(e, "age");
            }}
          />
        </div>
        <div className="field-box field-box--checked">
          <label>Employed</label>
          <input
            type="checkbox"
            value={isEmployed}
            onChange={handleIsEmployeedChange}
          />
        </div>
        <div className="field-box">
          <label>Favourite Color</label>
          <select
            value={data.color}
            onChange={(e) => {
              handleInputField(e, "color");
            }}
          >
            <option value=""></option>
            <option value="red">Red</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
          </select>
        </div>
        <div className="field-box field-box--check-sauce">
          <label className="m-right">Sauces</label>
          <div className="align-box">
            {checkItems.map((item, index) => {
              return (
                <label key={index}>
                  <input
                    type="checkbox"
                    value={item}
                    onChange={handleCheckListChange}
                  />
                  {item}
                </label>
              );
            })}
          </div>
        </div>
        <div className="field-box field-box--check-sauce">
          <label className="m-right--radio">Best Stooge</label>
          <div className="align-box">
            {radioItems.map((radioItem, id) => {
              return (
                <label key={id} className="radioLabel">
                  <input
                    type="radio"
                    checked={data.radio === radioItem}
                    value={radioItem}
                    onChange={(e) => {
                      handleInputField(e, "radio");
                    }}
                  />
                  {radioItem}
                </label>
              );
            })}
          </div>
        </div>
        <div className="field-box ">
          <label>Textarea</label>
          <textarea
            maxLength={5}
            id="notes"
            name="notes"
            placeholder="Notes"
            value={data.notes}
            onChange={(e) => {
              handleInputField(e, "notes");
            }}
          />
        </div>
        <div className="field-box field-box--btn">
          <button
            className="submit-btn"
            type="submit"
            disabled={data.firstname === "" && data.lastname === "" && data.age === ""}
            value={!btnDisabled}
          >Submit
          </button>
          
          <button
            className="reset-btn"
            value={!btnDisabled}
            type="reset"
            disabled={btnDisabled}
          >Reset
          </button>
        </div>
        <div className="field-box field-box--gray">
          <pre>{JSON.stringify(jsonData, undefined, 2)}</pre>
        </div>
      </form>
    </div>
  );
};

export default User;
